$(document).ready(function () {

    /*fancybox*/

    $('[data-fancybox="bc-gallery"]').fancybox({
        loop: true
    });


    /* owl carousel
    var owl = $('.owl-carousel');
    owl.owlCarousel({
        loop:true,
        center: true,
        margin:50,
        navContainer: '.carousel-nav',
        responsiveClass:true,
        responsive:{
            0:{
                items:1,
                nav:true,
                dots:false
            },
            500:{
                items:2,
                nav:true,
                dots:false
            },
            750:{
                items:3,
                nav:true,
                dots:false
            },
            1400:{
                items:4,
                nav:true,
                dots:false
            }
        }
    });
    owl.on('mousewheel', '.owl-stage', function (e) {  //add mouswheel scrolling
        if (e.deltaY>0) {
            owl.trigger('next.owl');
        } else {
            owl.trigger('prev.owl');
        }
        e.preventDefault();
    });*/


    /* sticky menu*/
    $("#sticker").sticky({
        topSpacing: 0,
        zIndex: 1010
    });


    /* masked Input */
    $(".phonemask").mask("+7(999) 999-99-99");

    // <!--ajax form submit-->

    $('form.ajax-form').on('submit', function (e) {
        e.preventDefault();
        let form = $(this);
        console.log("Try sending")

        $.ajax({
            type: form.attr('method'),
            url: form.attr('action'),
            data: form.serialize(),
            // dataType: 'json',
            success: function (response) {
                let msg = JSON.parse(response);
                console.log("Message was send");
                $('.modal').modal('hide');
                $('#success-send').modal('show')
                form.find("textarea, input").val('');
                console.log(msg)
                if (parseInt(msg['error']) === 0) {
                    console.log("Message success send")
                    $('#success-send').modal('show')
                    // form.addClass('ajax-form-successful');
                    $('input, textarea, button', form).attr('disabled', true);
                }
            }
        });

        Comagic.addOfflineRequest({
            name: form.find('input[name="name"]').val(),
            email: form.find('input[name="email"]').val(),
            phone: form.find('input[name="phone"]').val(),
            message: 'Тип площади: ' + form.find('input[name="areaType"]:checked').val() + '; Площадь (до): ' + form.find('input[name="square"]:checked').val()
        });

        return false;
    });

    /*change color*/
    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {


        var prevBcName = $(e.relatedTarget).attr('id');
        var curBcName = $(e.target).attr('id');
        var prevList = $('.' + prevBcName + '');
        prevList.each(function (element) {
            $(this).removeClass(prevBcName).addClass(curBcName);

        });
        if (prevBcName !== 'theme-iqpark') {
            $('.carousel-block').find('.theme-iqpark').removeClass('theme-iqpark').addClass(curBcName);
        }

    });

    /* info offer */

    $('#form-long').on('show.bs.modal', function (e) {
        var button = $(e.relatedTarget);
        $('#item-descr').val(button.siblings('.description-area').text() + '\n'
            + button.siblings('.description-price').text() + '\n'
            + button.siblings('.description-metro').text())

    });

function getAllUrlParams(url) {

  // get query string from url (optional) or window
  var queryString = url ? url.split('?')[1] : window.location.search.slice(1);

  // we'll store the parameters here
  var obj = {};

  // if query string exists
  if (queryString) {
        // stuff after # is not part of query string, so get rid of it
        queryString = queryString.split('#')[0];

        // split our query string into its component parts
        var arr = queryString.split('&');

        for (var i = 0; i < arr.length; i++) {
          // separate the keys and the values
          var a = arr[i].split('=');

          // set parameter name and value (use 'true' if empty)
          var paramName = a[0];
          var paramValue = typeof (a[1]) === 'undefined' ? true : a[1];

          // (optional) keep case consistent
          paramName = paramName.toLowerCase();
          if (typeof paramValue === 'string') paramValue = paramValue.toLowerCase();

          // if the paramName ends with square brackets, e.g. colors[] or colors[2]
          if (paramName.match(/\[(\d+)?\]$/)) {

            // create key if it doesn't exist
            var key = paramName.replace(/\[(\d+)?\]/, '');
            if (!obj[key]) obj[key] = [];

            // if it's an indexed array e.g. colors[2]
            if (paramName.match(/\[\d+\]$/)) {
              // get the index value and add the entry at the appropriate position
              var index = /\[(\d+)\]/.exec(paramName)[1];
              obj[key][index] = paramValue;
            } else {
              // otherwise add the value to the end of the array
              obj[key].push(paramValue);
            }
          } else {
            // we're dealing with a string
            if (!obj[paramName]) {
              // if it doesn't exist, create property
              obj[paramName] = paramValue;
            } else if (obj[paramName] && typeof obj[paramName] === 'string'){
              // if property does exist and it's a string, convert it to an array
              obj[paramName] = [obj[paramName]];
              obj[paramName].push(paramValue);
            } else {
              // otherwise add the property
              obj[paramName].push(paramValue);
            }
          }
        }
      }

      return obj;
    }

    if (typeof getAllUrlParams().tab !== "undefined"){
        $query = getAllUrlParams().tab;
        $("#theme-"+$query).click();
    }

});


var scroll = new SmoothScroll('a[href="#offers"]', {
    speed: 500,
    speedAsDuration: true,
    header: '[data-scroll-header]'
});






