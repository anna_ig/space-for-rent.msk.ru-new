<?php
// def response
$response = array(
    'message' => '',
    'error' => 1
);

$to      = 'sky.property@mail.ru';
$to     .= ',m.mikhaleva@registratura.ru';
$subject = 'Заявка с сайта space-for-rent.msk.ru';

$message = '';

$name = $_POST['name'];
if (($name) != '') {
	$message .= "<strong>Имя</strong>: " . $name . "<br/>";
}

$phone = $_POST['phone'];
if (($phone) != '') {
	$message .= "<strong>Телефон</strong>: " . $phone . "<br/>";
}

$email = $_POST['email'];
if (($email) != '') {
	$message .= "<strong>Почта</strong>: " . $email . "<br/>";
}

$areaType = $_POST['areaType'];
if (($areaType) != '') {
	$message .= "<strong>Тип площади</strong>: " . $areaType . "<br/>";
}

$square = $_POST['square'];
if (($square) != '') {
	$message .= "<strong>Размер площади</strong>: " . $square . "<br/>";
}

$headers  = 'From: noreply@space-for-rent.msk.ru' . "\r\n";
$headers .= 'MIME-Version: 1.0' . "\r\n";
$headers .= "Content-type: text/html; charset=\"utf-8\"";

if (mail($to, $subject, $message, $headers)) {
        $response['message'] = '';
        $response['error'] = 0;
}

die(json_encode($response));
