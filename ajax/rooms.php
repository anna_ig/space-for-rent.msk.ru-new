<?php

$result = array();

$tmp_file = 'freerooms_tmp.xml';
if (file_exists($tmp_file)) {
    $stat = stat($tmp_file);
    if (($stat['mtime'] + 3600) < time()) {
        unlink($tmp_file);
    }
}

if (!file_exists($tmp_file)) {
    file_put_contents($tmp_file, file_get_contents('http://sky-arenda.ru/freerooms/freerooms.xml'));
}

if (filesize($tmp_file)) {

    $xml = simplexml_load_file($tmp_file);

    // типы помещений, названия
    $arTypes = array(
        'SCLAD'    => 'Склад',
        'OFFICE'   => 'Офис',
        'TRADE'    => 'Торговое',
        'INDUSTRY' => 'Производство',
    );

    foreach ($xml as $Object) {

        if ( !empty($_REQUEST['code']) && !in_array((string)$Object->code, explode(',', $_REQUEST['code'])) ) {
            continue;
        }

        foreach($Object->rooms->room as $Room) {
            $result[] = array(
                'id'        => (string)$Room->id,
                'bc_code'   => (string)$Room->bc_code,
                'type_name' => ((isset($arTypes[(string)$Room->type_code])) ? $arTypes[(string)$Room->type_code] : 'Помещение'),
                'square'    => (string)$Room->square,
                'price'     => (string)$Room->price,
                'photo'     => (string)$Room->photo,
                'metro'     => (string)$Object->metro,
            );
        }
    }

} // if (filesize($tmp_file))

die(json_encode($result));